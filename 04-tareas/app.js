require('colors');
const { guardarDB, leerDB } = require('./helpers/guardarArchivo');
const { inquirerMenu, pausa, leerInput, listadoTareasBorrar, confirmar, mostrarListadoCheckList } = require('./helpers/inquirer');
const Tareas = require('./models/tareas');

console.clear();

const main = async () => {
    let opt = '';
    const tareas = new Tareas();
    const tareasDB = leerDB();

    if ( tareasDB ) {
        tareas.cargarTareas(tareasDB);
    }
    
    do {
        opt = await inquirerMenu();

        switch (opt) {
            case '1':
                //Crear tarea
                const desc = await leerInput('Descripcion:');
                tareas.crearTarea(desc);
                break;
            case '2':
                tareas.listadoCompleto()
                break;
            case '3':
                tareas.listarPorEstatus()
                break;
            case '4':
                tareas.listarPorEstatus(false)
                break;
            case '5':
                const ids = await mostrarListadoCheckList(tareas.listadoArr);
                tareas.toggleCompletadas(ids);
                break;
            case '6':
                const id = await listadoTareasBorrar(tareas.listadoArr);
                if ( id !== '0') {
                    const ok = await confirmar('¿Estas seguro de eliminar la tarea?');
                    if ( ok ) tareas.borrarTarea(id);
                }
                break;
        }

        guardarDB(tareas.listadoArr);

        await pausa();
    } while ( opt !== '0' );
}

main();