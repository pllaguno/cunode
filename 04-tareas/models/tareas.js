require('colors');
const Tarea = require("./tarea");

class Tareas {
    _listado = {};

    get listadoArr() {
        const listado = [];
        Object.keys(this._listado).forEach(t => {
            const tarea = this._listado[t]
            listado.push( tarea );
        })
        return listado
    }

    constructor() {
        this._listado = {};
    }

    borrarTarea( id = '' ) {
        if ( this._listado[id] ) {
            delete this._listado[id];
        }
    }

    cargarTareas( tareas = [] ) {
        tareas.forEach(tarea => {
            this._listado[tarea.id] = tarea;
        })
    }

    crearTarea( desc = '' ) {
        const tarea = new Tarea(desc);
        this._listado[tarea.id] = tarea;
    }

    listadoCompleto() {
        console.log();
        this.listadoArr.forEach((tarea, index) => {
            //* Forma 1
            // let estatus = tarea.completadoEn === null ? 'Pendiente'.red : 'Completada'.green;
            // console.log(`${`${(index+1)}:`.green} ${tarea.desc} :: ${estatus}`);
            //* Forma 2
            let idx = `${index + 1}:`.green;
            let { desc, completadoEn } = tarea;
            let estatus = completadoEn ? `${completadoEn}`.green : 'Pendiente'.red;
            console.log(`${idx} ${desc} :: ${estatus}`);
        });
        console.log();
    }

    listarPorEstatus( completadas = true) {
        console.log();
        this.listadoArr.forEach((tarea, index) => {
            let idx = `${index + 1}:`.green;
            let { desc, completadoEn } = tarea;
            let estatus = completadoEn ? `${completadoEn}`.green : 'Pendiente'.red;
            if ( completadas && completadoEn ) {
                console.log(`${idx} ${desc} :: ${estatus}`);
            }
            if ( !completadas && !completadoEn ) {
                console.log(`${idx} ${desc} :: ${estatus}`);
            }
        });
        console.log();
    }

    toggleCompletadas( ids = [] ) {
        ids.forEach(id => {
            const tarea = this._listado[id];
            if ( !tarea.completadoEn ) tarea.completadoEn = new Date().toISOString();
        })

        this.listadoArr.forEach( tarea => {
            if( !ids.includes(tarea.id) ) this._listado[tarea.id].completadoEn = null;
        });
    }

}

module.exports = Tareas;