// setTimeout( function(){
//     console.log('Hola Mundo')
// }, 1000);

//* Los callback son funciones que se mandan como argumentos a otra fucion */

const getUsuarioByID = (id, callback) => {
    const usuario = {
        id,
        nombre: 'Pavel'
    }

    setTimeout( () => {
        callback(usuario)
    }, 1500 );
}

getUsuarioByID(10, ({id, nombre}) => {
    console.log(id);
    console.log(nombre.toUpperCase());
});