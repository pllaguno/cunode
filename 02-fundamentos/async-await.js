const empleados = [
    { id: 1, nombre: 'Pavel' },
    { id: 2, nombre: 'Lidia' },
    { id: 3, nombre: 'Rich' }
];

const salarios = [
    { id: 1, salario: 1000 },
    { id: 2, salario: 1000 }
];

const getEmpleado = ( id ) => { 
    return new Promise( (resolve, reject) => {
        const empleado = empleados.find( (e) => e.id === id )?.nombre
        empleado ? resolve(empleado) : reject('No existe empleado');
    } );
}

const getSalario = ( id ) => {
    return new Promise( (resolve, reject) => {
        const salario = salarios.find( (s) => s.id === id )?.salario
        salario ? resolve(salario) : reject('No existe salario');
    } );
}

const getInfoUsuario = async ( id ) => {
    try {
        const nombre = await getEmpleado(id);
        const salario = await getSalario(id)
        return `El salario de ${nombre} es de ${salario} dolares.`
    } catch (error) {
        throw error;
    }
}

const id = 1;

getInfoUsuario(id)
    .then( msg => console.log(msg))
    .catch( err => console.log(err));


