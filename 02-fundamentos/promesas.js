const empleados = [
    { id: 1, nombre: 'Pavel' },
    { id: 2, nombre: 'Lidia' },
    { id: 3, nombre: 'Rich' }
];

const salarios = [
    { id: 1, salario: 1000 },
    { id: 2, salario: 1000 }
];

const id = 3;

const getEmpleado = ( id ) => { 
    return new Promise( (resolve, reject) => {
        const empleado = empleados.find( (e) => e.id === id )?.nombre
        empleado ? resolve(empleado) : reject('No existe empleado');
    } );
}

const getSalario = ( id, empleado ) => {
    return new Promise( (resolve, reject) => {
        const salario = salarios.find( (s) => s.id === id )?.salario
        salario ? resolve(salario) : reject('No existe salario');
    } );
}

// getEmpleado(id)
//     .then( empleado => console.log(empleado) )
//     .catch( err => console.log(err) );

// getSalario(id)
//     .then( salario => console.log(salario) )
//     .catch( err => console.log(err) );

// getEmpleado(id)
//     .then( empleado => {
//         getSalario(id)
//             .then(salario => console.log(empleado, salario))
//             .catch(err => console.log(err));
//     } )
//     .catch( err => console.log(err) );

let nombre;

getEmpleado(id)
    .then( empleado => {
        nombre = empleado;
        return getSalario(id);
    })
    .then( salario => console.log('Empleado:', nombre, 'Salario:', salario) )
    .catch( err => console.log(err));