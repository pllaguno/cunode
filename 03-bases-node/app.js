const { crearArchivoMultiplicar } = require('./helpers/multiplicar')
const argv = require('./config/yargs');
const colors = require('colors');

console.clear();

// const [,,arg3 = 'base=0'] = process.argv;
// const [,base = 0] = arg3.split('=');

// console.log(argv);

// const base = 7;

crearArchivoMultiplicar(argv.b, argv.l, argv.h)
        .then( archivo => console.log(archivo.trap, 'creado'.trap))
        .catch(err => console.log(err));


