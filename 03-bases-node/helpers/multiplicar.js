const fs = require('fs');
const colors = require('colors');

const crearArchivoMultiplicar = async (base, list, hasta) => {
    console.log(`============= Tabla del ${base} ==============`.zebra)
    try {
        let salida = '';
        for (let i = 1; i <= hasta; i++) {
            salida += (`${base} x ${i} = ${base*i}\n`);
        }
        if (list) console.log(salida.rainbow);
        fs.writeFileSync(`./salida/tabla-${base}.txt`, salida);
        return(`tabla-${base}.txt`);
    } catch (err) {
        throw(err);
    }
}

module.exports = {
    crearArchivoMultiplicar
}